Magnet data
==========

Magnet data is a Django application that handles currency changes
and chilean holidays

Magnet data supports the following currencies:
* CLP
* CLF
* UTM
* USD
* EUR

How to install
--------------

You can install *magnet data* with regular *pip* command

```
$ pip install magnet_data
```
To install the current development version from GitHub, use:

```
$ pip install git+https://bitbucket.org/magnet-cl/magnet_data.git#egg=magnet_data
```

Then add *magnet_data* to *INSTALLED_APPS*:
```python
INSTALLED_APPS = (
    ...
    'magnet_data'.
    ...
)
```

And finally create the models:

```
$ python manage.py migrate magnet_data
```

How works?
----------

The first time you request a value, you will get the value of our API and the result will be saved in your database.
The following queries for the same value will be obtained from your database.

Get currencies is easy
----------------------
Magnet data uses *datetime.date* objects

```python
import datetime

from magnet_data.models import Currency

# get today's USD value
usd_value = Currency.get_value('USD')
# from another date
another_date = datetime.date(2010,1,1)
eur_value = Currency.get_value('EUR', another_date)
```

You can update currencies values for a specific day and save the values in your database.


```python
# all currencies
Currency.update_currencies() # today
Currency.update_currencies(date)

# specific currency
Currency.update_currency('USD', date)
# with a currency instance
USD = Currency.get_usd() # instance
USD.update_VALUES(date)
```

Also, you can convert an amount to another currency.

```python
USD = Currency.get_usd()
USD.convert('CLP', 2000, date)
```
