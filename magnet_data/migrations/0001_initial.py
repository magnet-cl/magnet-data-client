# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='The name of the currency', unique=True, max_length=255, verbose_name='name')),
                ('acronym', models.CharField(help_text='The acronym of the currency', max_length=5, verbose_name='acronym')),
                ('symbol', models.CharField(help_text='The symbol of the currency', max_length=3, verbose_name='symbol')),
                ('svs_name', models.CharField(help_text='The SVS name of the currency', max_length=255, null=True, verbose_name='svs name')),
            ],
        ),
        migrations.CreateModel(
            name='CurrencyValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.FloatField(default=0, verbose_name='value')),
                ('date', models.DateField(help_text='The currency date', verbose_name='currency date')),
                ('currency', models.ForeignKey(related_name='values', verbose_name='currency', to='magnet_data.Currency')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='currencyvalue',
            unique_together=set([('currency', 'date')]),
        ),
    ]
