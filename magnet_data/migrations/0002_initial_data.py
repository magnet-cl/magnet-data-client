# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def load_currencies(apps, schema_editor):
    Currency = apps.get_model("magnet_data", "Currency")
    clf = Currency(
        acronym="CLF",
        symbol="UF",
        name=" Unidad de Fomento"
    )
    clf.save()
    usd = Currency(
        acronym="USD",
        symbol="US$",
        name="Dólar Estadounidense"
    )
    usd.save()
    eur = Currency(
        acronym="EUR",
        symbol="€",
        name="Euro"
    )
    eur.save()
    clp = Currency(
        acronym="CLP",
        symbol="$",
        name="Peso"
    )
    clp.save()
    utm = Currency(
        acronym="UTM",
        symbol="UTM",
        name="Unidad Tributaria Mensual"
    )
    utm.save()


class Migration(migrations.Migration):

    dependencies = [
        ('magnet_data', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_currencies),
    ]
