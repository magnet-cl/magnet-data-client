# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('magnet_data', '0003_holiday'),
    ]

    operations = [
        migrations.CreateModel(
            name='HealthCareCompany',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='The name of the health care company', max_length=100, verbose_name='name')),
                ('rut', models.CharField(null=True, max_length=20, blank=True, help_text='Health Care Company RUT', unique=True, verbose_name='rut', db_index=True)),
            ],
            options={
                'verbose_name_plural': 'health care companies',
            },
        ),
        migrations.CreateModel(
            name='PensionFundAdministrator',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text='The name of the pension fund administrator', max_length=100, verbose_name='name')),
                ('rut', models.CharField(null=True, max_length=20, blank=True, help_text='Pension Fund Administrator RUT', unique=True, verbose_name='rut', db_index=True)),
                ('fee', models.DecimalField(decimal_places=2, max_digits=5, blank=True, help_text='The value of pension fund administrator fee', null=True, verbose_name='fee')),
            ],
            options={
                'verbose_name_plural': 'pension fund administrators',
            },
        ),
    ]
