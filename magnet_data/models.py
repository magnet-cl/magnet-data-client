# -*- coding: utf-8 -*-

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

# utils
import utils

# standard library
import datetime
import json
from urllib2 import Request
from urllib2 import urlopen


class Currency(models.Model):
    # initial currencies ids
    CLF = 'CLF'
    USD = 'USD'
    EUR = 'EUR'
    CLP = 'CLP'
    UTM = 'UTM'

    AUTOMATED_CURRENCIES_API_URLS = (
        (CLF, 'https://data.magnet.cl/api/v1/currencies/clf/clp/{}/{}/'),
        (USD, 'https://data.magnet.cl/api/v1/currencies/usd/clp/{}/{}/'),
        (EUR, 'https://data.magnet.cl/api/v1/currencies/eur/clp/{}/{}/'),
    )

    MONTHLY_CURRENCIES_API_URLS = (
        (UTM, 'https://data.magnet.cl/api/v1/currencies/utm/clp/{}/{}/'),
    )

    API_URLS = AUTOMATED_CURRENCIES_API_URLS + MONTHLY_CURRENCIES_API_URLS

    NON_MODIFIABLE_CURRENCIES = (
        CLF,
        USD,
        EUR,
        CLP,
        UTM,
    )

    # required fields
    name = models.CharField(
        _('name'),
        max_length=255,
        unique=True,
        help_text=_("The name of the currency"),
    )
    acronym = models.CharField(
        _('acronym'),
        max_length=5,
        help_text=_("The acronym of the currency"),
    )
    symbol = models.CharField(
        _('symbol'),
        max_length=3,
        help_text=_("The symbol of the currency"),
    )

    # optional fields
    svs_name = models.CharField(
        _('svs name'),
        max_length=255,
        null=True,
        help_text=_("The SVS name of the currency"),
    )

    def __unicode__(self):
        return self.symbol

    def __get_api_url(self):
        for acronym, api_url in self.API_URLS:
            if acronym == self.acronym:
                return api_url

        return None

    @classmethod
    def update_currency(cls, acronym, date=None):
        if acronym in cls.API_URLS:
            currency = Currency.objects.get(acronym=acronym)
            currency.update_values(date)

    @classmethod
    def update_currencies(cls, date=None):
        for acronym, base_url in cls.API_URLS:
            currency = Currency.objects.get(acronym=acronym)
            currency.update_values(date)

    def update_values(self, date=None):
        def exist_value(tuple_list, value):
            for x, y in tuple_list:
                if x == value:
                    return True
            return False

        base_url = self.__get_api_url()
        if exist_value(Currency.AUTOMATED_CURRENCIES_API_URLS, self.acronym):
            Currency.update_values_daily(self, base_url, date)
        elif exist_value(Currency.MONTHLY_CURRENCIES_API_URLS, self.acronym):
            Currency.update_values_monthly(self, base_url, date)

    @classmethod
    def update_values_daily(cls, currency, base_url, date=None):
        if date is None:
            month = datetime.date.today().month
            year = datetime.date.today().year
        else:
            month = date.month
            year = date.year

        if month < 10:
            _month = '0{}'.format(month)

        url = base_url.format(year, _month)
        request = Request(url.format(year, _month))

        response = urlopen(request)
        data = json.loads(response.read())

        for values_data in data['objects']:
            date_string = values_data['date']
            date = datetime.datetime.strptime(
                date_string, '%Y-%m-%d'
            ).date()

            currency_value, created = CurrencyValue.objects.get_or_create(
                date=date,
                currency=currency
            )

            currency_value.value = values_data['value']
            currency_value.save()

    @classmethod
    def update_values_monthly(cls, currency, base_url, date=None):
        if date is None:
            month = datetime.date.today().month
            year = datetime.date.today().year
        else:
            month = date.month
            year = date.year

        if month < 10:
            _month = '0{}'.format(month)

        url = base_url.format(year, _month)
        request = Request(url.format(year, _month))

        response = urlopen(request)
        data = json.loads(response.read())

        # first day of the month
        date = datetime.date(year, month, 1)

        currency_value, created = CurrencyValue.objects.get_or_create(
            date=date,
            currency=currency
        )
        currency_value.value = data['value']
        currency_value.save()

    @classmethod
    def get_clp(cls):
        return cls.objects.get(acronym=cls.CLP)

    @classmethod
    def get_clf(cls):
        return cls.objects.get(acronym=cls.CLF)

    @classmethod
    def get_usd(cls):
        return cls.objects.get(acronym=cls.USD)

    @classmethod
    def get_eur(cls):
        return cls.objects.get(acronym=cls.EUR)

    @classmethod
    def get_utm(cls):
        return cls.objects.get(acronym=cls.UTM)

    @classmethod
    def get_value(cls, acronym, date=None):
        """ returns the value of a currency for a given date """
        if acronym == cls.CLP:
            return 1

        if date is None:
            date = utils.today()

        if acronym in cls.MONTHLY_CURRENCIES_API_URLS:
            date = datetime.date(date.year, date.month, 1)

        try:
            currency = CurrencyValue.objects.get(
                currency__acronym=acronym,
                date=date)
        except CurrencyValue.DoesNotExist:
            if acronym in (cls.CLF, cls.USD, cls.EUR, cls.UTM):
                try:
                    Currency.update_currency(acronym, date)
                except Currency.DoesNotExist:
                    # if the currency is not registered in the database
                    # then this must be a test, or we allowed the
                    # on of the primary currencies to be deleted.
                    return 1

                currency = CurrencyValue.objects.get(
                    currency__acronym=acronym,
                    date=date)
            else:
                if date <= utils.today():
                    # create a new value from the previous one
                    currency_value = CurrencyValue.objects.filter(
                        currency__acronym=acronym,
                        date__lt=date
                    ).order_by('date').last()
                    if currency_value:
                        currency_value.id = None
                        currency_value.date = date
                        currency_value.save()
                        return currency_value.value

                return 1

        return currency.value

    def converter(self, amount, to_acronym=CLF, date=None):
        """ converts the given amount from this currency to the
        target currency "to_acronym" (Default to CLF)
        """

        from_acronym = str(self.acronym)

        # equal currencies
        if from_acronym == to_acronym:
            return amount

        # set date
        if date is None:
            date = utils.today()

        # from CLP
        if from_acronym == Currency.CLP:
            to_currency = Currency.get_value(to_acronym, date)

            from_amount = amount / to_currency
            return from_amount

        # to CLP
        elif to_acronym == Currency.CLP:
            from_currency = Currency.get_value(from_acronym, date)

            from_amount = amount * from_currency
            return from_amount

        # dfferent currencies
        else:
            to_currency = Currency.get_value(to_acronym, date)

            from_currency = Currency.get_value(from_acronym, date)

            from_amount = amount / to_currency
            to_amount = from_amount * from_currency

            return to_amount


class CurrencyValue(models.Model):
    # required fields
    value = models.FloatField(
        _('value'),
        default=0,
    )
    date = models.DateField(
        verbose_name=_("currency date"),
        help_text=_("The currency date"),
    )

    # foreign keys
    currency = models.ForeignKey(
        Currency,
        verbose_name=_("currency"),
        related_name='values',
    )

    class Meta:
        unique_together = (
            ('currency', 'date'),
        )


class Holiday(models.Model):
    """ A model for a holiday"""
    name = models.CharField(
        verbose_name=_("name"),
        max_length=255,
    )

    date = models.DateField(
        _('date'),
        help_text=_("The holiday date"),
    )

    def __unicode__(self):
        return self.date.strftime('%Y-%m-%d')

    @classmethod
    def update_holidays(cls, year=None):
        if year is None:
            year = datetime.date.today().year

        request = Request(
            'https://data.magnet.cl/api/v1/holidays/cl/{}/'.format(year))

        response = urlopen(request)
        data = json.loads(response.read())

        for holiday_data in data['objects']:
            date_string = holiday_data['date']
            date = datetime.datetime.strptime(date_string, '%Y-%m-%d').date()
            name = holiday_data['name']
            cls.objects.get_or_create(date=date, name=name)

        year += 1

        request = Request(
            'https://data.magnet.cl/api/v1/holidays/cl/{}/'.format(year))

        response = urlopen(request)
        data = json.loads(response.read())

        for holiday_data in data['objects']:
            date_string = holiday_data['date']
            date = datetime.datetime.strptime(date_string, '%Y-%m-%d').date()
            name = holiday_data['name']
            cls.objects.get_or_create(date=date, name=name)

    @classmethod
    def is_workday(cls, date=None):
        """
        Returns True if the given date is not Saturday, Sunday, or
        Holiday
        """
        if date is None:
            date = datetime.date.today()

        if date.weekday() == 5:  # Saturday
            return False

        if date.weekday() == 6:  # Sunday
            return False

        if Holiday.objects.filter(date=date).count():
            return False

        return True

    @classmethod
    def is_holiday(cls, date=None):
        """
        Returns True if the given date is a Holiday
        """
        if date is None:
            date = datetime.date.today()

        if Holiday.objects.filter(date=date).count():
            return True

        return False

    @classmethod
    def get_next_working_day(cls, working_days=1, from_date=None):
        def is_workday(date):
            """
            Returns True if the given date is not Saturday, Sunday, or
            Holiday
            """
            if date.weekday() == 5:  # Saturday
                return False

            if date.weekday() == 6:  # Sunday
                return False

            if Holiday.objects.filter(date=date).count():
                return False

            return True

        if from_date is None:
            from_date = timezone.now()

        final_date = from_date
        while working_days:
            final_date += datetime.timedelta(days=1)

            if is_workday(final_date):
                working_days -= 1

        return final_date

    @classmethod
    def get_holidays(cls, start_date, end_date):

        holidays = cls.objects.filter(
            date__range=[start_date, end_date]).count()

        return holidays

    @classmethod
    def get_holidays_count_during_weekdays(cls, start_date, end_date):
        days = 0

        holidays_dates = cls.objects.filter(
            date__range=[start_date, end_date]).values_list('date', flat=True)
        for date in holidays_dates:
            if date.weekday() not in (5, 6):
                days += 1

        return days

    @classmethod
    def get_weekend_days(cls, start_date, end_date):
        # increase end date to include it on the time date range
        end_date = end_date + datetime.timedelta(1)

        days = 0
        for i in xrange(int((end_date - start_date).days)):
            current_date = start_date + datetime.timedelta(i)
            if current_date.weekday() in (5, 6):
                days += 1
        return days


class HealthCareCompany(models.Model):
    name = models.CharField(
        _('name'),
        help_text=_('The name of the health care company'),
        max_length=100,
    )
    rut = models.CharField(
        _('rut'),
        unique=True,
        db_index=True,
        help_text=_('Health Care Company RUT'),
        max_length=20,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name_plural = _('health care companies')

    # private methods
    def __unicode__(self):
        return self.name

    @classmethod
    def update_healthcare_data(cls):
        request = Request('https://data.magnet.cl/api/v1/prevision/health/')
        response = urlopen(request)
        data = json.loads(response.read())

        for healthcare_data in data['objects']:
            name = healthcare_data['name']
            healthcare, created = cls.objects.get_or_create(name=name)
            healthcare.save()


class PensionFundAdministrator(models.Model):
    name = models.CharField(
        _('name'),
        help_text=_('The name of the pension fund administrator'),
        max_length=100,
    )
    rut = models.CharField(
        _('rut'),
        unique=True,
        db_index=True,
        help_text=_('Pension Fund Administrator RUT'),
        max_length=20,
        null=True,
        blank=True,
    )
    fee = models.DecimalField(
        _('fee'),
        help_text=_('The value of pension fund administrator fee'),
        null=True,
        blank=True,
        decimal_places=2,
        max_digits=5,
    )

    class Meta:
        verbose_name_plural = _('pension fund administrators')

    # private methods
    def __unicode__(self):
        return self.name

    @classmethod
    def update_pension_fund_data(cls):
        request = Request('https://data.magnet.cl/api/v1/prevision/pension/')
        response = urlopen(request)
        data = json.loads(response.read())

        for pension_fund_data in data['objects']:
            fee = pension_fund_data['fee']
            name = pension_fund_data['name']
            pension_fund, created = cls.objects.get_or_create(name=name)
            pension_fund.fee = fee
            pension_fund.save()
