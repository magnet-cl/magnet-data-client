import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='magnet_data',
    version='0.2',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='An API client.',
    long_description=README,
    url='https://www.magnet.cl/',
    author='Magnet',
    author_email='admin@magnet.cl',
)
